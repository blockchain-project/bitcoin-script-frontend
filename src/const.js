export const ENDPOINTS = {
  TRANSACTION: (id, net) =>
    net === "main" ? `/transaction/${id}` : `/transaction/test/${id}`,
  VALIDATE: (id, net) =>
    net === "main"
      ? `/transaction/${id}/valid`
      : `/transaction/test/${id}/valid`,
  SCRIPTS: (id, net) =>
    net === "main"
      ? `/transaction/${id}/script`
      : `/transaction/test/${id}/script`,
  VISUALIZE_SCRIPT: "/visualize_script",
  PARSE_OPCODE_STR: "/opcodes"
}

export const CLASSIFICATIONS = {
  pay_to_public_key: {
    name: "Pay to Pubkey (P2PK)",
    description:
      "Simple script to send bitcoin to single address. To unlock it, a valid signature needs to be provided. P2PK was used primarly in the early days of Bitcoin.",
  },
  pay_to_public_key_hash: {
    name: "Pay to PubKey Hash (P2PKH)",
    description:
      "Standard way to send Bitcoin from a single address to a single address. To unlock it, both the original public key and a valid signature need to be provided.",
  },
  checkmultisig: (data) => {
    return {
      name: "Multi-signature Script",
      description: `${data[0]}-of-${data[1]} Mulit-Signature: of ${data[1]} authorized signatures, ${data[0]} are needed.`,
    }
  },
  pay_to_script_hash: {
    name: "Pay to Script Hash (P2SH)",
    description:
      "Lock output with the hash of a redeem script hash known by the Bitcoin receiver. To unlock it, the receiver provides redeem script and necessary arguments. Other script types can be wrapped in a P2SH script in this way.",
  },
  pay_to_witness_public_key_hash: {
    name: "Pay-to-Witness-Public-Key-Hash (P2WPKH)",
    description:
      "In a segregated witness (segwit) script, the unlocking script of the output is provided as a separate witness. To make it backwards-compatible, the locking script consist of two values that are pushed: the witness version and the witness program. The 20-byte witness program is the hash of the public key.",
  },
  pay_to_witness_script_hash: {
    name: "Pay-to-Witness-Script-Hash (P2WSH)",
    description:
      "In a segregated witness (segwit) script, the unlocking script of the output is provided as a separate witness. To make it backwards-compatible, the locking script consist of two values that are pushed: the witness version and the witness program. The 32-byte witness program is the hash of the redeem script.",
  },
  null_data_unspendable: (data) => {
    return {
      name: `Null Data, Unspendable (OP RETURN)${
        data && data.protocol ? "; Detected Protocol: " + data.protocol : null
      }`,
      description:
        "Include abitrary data on the blockchain in exchange for paying a transaction fee. The output cannot be spent. The data portion is limited to 80 bytes and most often represents a hash, such as the output from the SHA256 algorithm (32 bytes). Many applications put a prefix in front of the data to help identify the application or protocol used. For example, the Proof of Existence digital notarization service uses the 8-byte prefix DOCPROOF, which is ASCII encoded as 44 4f 43 50 52 4f 4f 46 in hexadecimal.",
    }
  },
  unknown: (data) => {
    return {
      name: `${
        data && data.type && data.type === "coinbase" ? "Coinbase" : "Unknown"
      }`,
    }
  },
}

// The types of scripts embedded in an input_script or output_script
export const SCRIPT_TYPES = {
  prevout: {
    name: "Previous Output Script",
    description:
      "This is the unlocking script that corresponds to the output that is consumed by this input. Its classification is therefore the same as that of the input script.",
  },
  inner_redeemscript: {
    name: "Inner Redeem Script",
  },
  inner_witnessscript_asm: {
    name: "Inner Witness Script",
  },
}

export const TOOLTIP_TRANSACTION_FEE = "The fee left for the miner by this transaction."
export const TOOLTIP_TRANSACTION_WEIGHT = "The relative weight of this transaction in comparison to the total blocksize the transaction is in."
