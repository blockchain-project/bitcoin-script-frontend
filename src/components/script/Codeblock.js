import PropTypes from "prop-types"
import React from "react"
import styles from "./Codeblock.module.scss"

function Codeblock(props) {
  const { opcodes, header } = props
  // opcodes: [{code: str, data: [str]}]

  return (
    // Container
    <div className={styles.body}>
      {/* Script */}
      <pre className={styles.code}>
        {/* Header Line */}
        {header ? (
          <span className={`${styles.line} ${styles.lineHeader}`}>
            <span className={styles.lineNumber}> </span>
            {header}
          </span>
        ) : null}

        {/* For every line in script */}
        {opcodes.map((item, i) => (
          // Line formatting
          <span key={i} className={styles.line}>
            {/* Line Number */}
            <span className={styles.lineNumber}>{++i}</span>
            {/* OP_CODE */}
            <span>{item.code}</span>

            {/* DATA */}
            {item.data && item.data.length > 0
              ? item.data.map((param, i) => (
                <span key={i} className={styles.data}>
                  {` ${param}`}
                </span>
              ))
              : null}
          </span>
        ))}

        {/* {[>Run Script Button<]}
        <div className={styles.footer}>
          <button
            className="btn btn-primary"
            onClick={() => history.push(`/sandbox?opcode_str=${opcode_str}`)}>
            Run Script
          </button>
        </div> */}
      </pre>
    </div>
  )
}

Codeblock.propTypes = {
  opcodes: PropTypes.array.isRequired,
  header: PropTypes.any,
}

export default Codeblock
