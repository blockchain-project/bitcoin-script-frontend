import PropTypes from "prop-types"
import React from "react"
import { CLASSIFICATIONS, SCRIPT_TYPES } from "../../const"
import Codeblock from "./Codeblock"
import styles from "./Script.module.scss"
import { useHistory } from "react-router"

function Script(props) {
  const classification = props.classifier.data
    ? CLASSIFICATIONS[props.classifier.classification](props.classifier.data)
    : CLASSIFICATIONS[props.classifier.classification]

  const history = useHistory()

  const calcFullScript = () => {
    if (props.type) return "error" // We are in an embedded script
    return `${props.opcode_str} ${props.prevout ? props.prevout.opcode_str : ""} ${props.inner_redeemscript ? props.inner_redeemscript.opcode_str : ""} ${props.inner_witnessscript_asm ? props.inner_witnessscript_asm.opcode_str : ""}`
  }

  return (
    <div>
      {/* Header: "Script {i}" */}
      <p className={`h5 ${styles.title}`}>{props.title}</p>

      {/* Codeblock */}
      <Codeblock
        opcodes={props.opcodes}
        header={props.header}
      />

      {/* Explanation Type: Extra explanation on script type (used for prevout) */}
      {SCRIPT_TYPES[props.type] && SCRIPT_TYPES[props.type].description ? (
        <div className="mb-2">{SCRIPT_TYPES.prevout.description}</div>
      ) : null}

      {/* Classification: only inner_redeemscript && inner_witnessscript_asm */}
      {!props.type || (props.type && props.type !== "prevout") ? (
        <table className="table table-borderless table-sm m-0 mb-2">
          <tbody>
            {/* Classification */}
            <tr>
              <th className="pl-0" scope="row">
                Classification
              </th>
              <td>{classification.name}</td>
            </tr>

            {/* Description of classification */}
            {classification.description ? (
              <tr className="m-0">
                <th className="pl-0" scope="row">
                  Description
                </th>
                <td>{classification.description}</td>
              </tr>
            ) : null}
          </tbody>
        </table>
      ) : null}

      {/* Render embedded scripts recursively */}
      {["prevout", "inner_redeemscript", "inner_witnessscript_asm"].map(
        (item, i) => (
          <div key={i}>
            {props[item] ? (
              <div className="ml-5">
                <Script
                  {...props[item]}
                  title={`${SCRIPT_TYPES[item].name}`}
                  type={item}
                  header={
                    <>
                      Script {props.index}: {item} -{" "}
                      {props[item].classifier.classification}
                    </>
                  }
                />
              </div>
            ) : null}
          </div>
        )
      )}

      {/* Run script button */}
      {props.runnable ?
        <div className="">
          <button className="btn btn-secondary w-100" onClick={() => history.push(`/sandbox?opcode_str=${calcFullScript()}`)}>
            Run Script
          </button>
        </div>
        : null}
    </div>
  )
}

Script.protoTypes = {
  opcodes: PropTypes.array.isRequired,
  opcode_str: PropTypes.string.isRequired,
  classifier: PropTypes.object.isRequired,

  prevout: PropTypes.object,
  inner_redeemscript_asm: PropTypes.object,
  inner_witnessscript_asm: PropTypes.object,
}

export default Script
