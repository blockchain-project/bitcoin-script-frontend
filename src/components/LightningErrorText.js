import PropTypes from "prop-types"
import React from "react"
import styled from "styled-components"

const LightningIcon = styled.i`
  color: red;
`

const LightningErrorText = (props) =>
  <div className={props.class}>
    <p className="text-center m-0">
      <LightningIcon className="bi bi-lightning-fill" /> {props.text}
    </p>
  </div>


LightningErrorText.propTypes = {
  class: PropTypes.any,
  text: PropTypes.string,
}

export default LightningErrorText
