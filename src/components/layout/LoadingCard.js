import React from "react"

export default function LoadingCard() {
  return (
    <div className="card w-100">
      <div className="card-body justify-content-center d-flex w-100 align-items-center">
        <span className="spinner spinner-border mr-3" />
        <span className="">Loading...</span>
      </div>
    </div>
  )
}
