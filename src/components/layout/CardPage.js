import React from "react"

export default function SingleColumnPage(props) {
  return (
    <div className="container">
      <div className="row pt-3 pb-3">
        <div className="col">{props.children}</div>
      </div>
    </div>
  )
}
