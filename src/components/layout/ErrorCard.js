import React from "react"
import PropTypes from "prop-types"

import LightningErrorText from "../LightningErrorText"

const ErrorCard = (props) =>
  <div className="card w-100 border-danger">
    <div className="card-body">
      <LightningErrorText text={props.text} />
    </div>
  </div>


ErrorCard.propTypes = {
  text: PropTypes.string.isRequired,
}

export default ErrorCard
