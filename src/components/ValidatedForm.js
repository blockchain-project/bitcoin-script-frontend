import PropTypes from "prop-types"
import React, { useState } from "react"
import { Form } from "react-bootstrap"

function ValidatedForm(props) {
  const [validated, setValidated] = useState(false)

  const handleSubmit = (event) => {
    const form = event.currentTarget
    event.preventDefault()
    event.stopPropagation()

    if (form.checkValidity() === true) {
      props.handleSubmit()
    }

    setValidated(true)
  }

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      {props.children}
    </Form>
  )
}

ValidatedForm.propTypes = {
  onSubmit: PropTypes.func,
}

export default ValidatedForm
