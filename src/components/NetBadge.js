import React from "react"
import styles from "./NetBadge.module.scss"

const NetBadge = (props) => {
  let net = props.net === "main" ? "bg-secondary" : "bg-warning"
  let netFriendly = props.net === "main" ? "MainNet" : "TestNet"

  return (
    <span className={`badge ${styles.badge} ${net}`}>
      {netFriendly}
    </span>
  )
}

export default NetBadge
