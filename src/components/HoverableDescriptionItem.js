import React from "react"
import PropTypes from "prop-types"
import {
  OverlayTrigger,
  Tooltip
} from "react-bootstrap"


const HoverableDescriptionItem = (props) =>
  <li className="list-group-item d-flex">
    {/* Key */}
    {props.tooltip ? (
      <OverlayTrigger
        placement="right"
        overlay={
          <Tooltip id={`tooltip-${props.name}`}>
            {props.tooltip}
          </Tooltip>
        }
      >
        {({ ref, ...triggerHandler }) => (<div className="col-3 p-0" style={{ "cursor": "pointer" }} {...triggerHandler}><b ref={ref}>{props.name}</b></div>)}
      </OverlayTrigger>
    ) : (
      <b className="col-3 p-0">{props.name}</b>
    )}

    {/* Value */}
    <span className="col-9 p-0">{props.description}</span>
  </li>


HoverableDescriptionItem.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.any.isRequired,
  tooltip: PropTypes.string,
}

export default HoverableDescriptionItem
