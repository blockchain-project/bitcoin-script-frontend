import React from "react"
import PropTypes from "prop-types"
import { Button } from "react-bootstrap"

function SpinnerButton(props) {
  let spinner = !props.loading ? "visually-hidden"
    : "d-flex mr-2 spinner-border spinner-border-sm"

  return (
    <Button
      className="d-flex align-items-center mr-0 ml-auto"
      disabled={props.loading}
      {...props}>

      <span
        className={spinner}
        role="status"
      />
      {props.children}
    </Button>
  )
}

SpinnerButton.propTypes = {
  loading: PropTypes.bool.isRequired,
  variant: PropTypes.any,
  type: PropTypes.any,
  children: PropTypes.any,
}

export default SpinnerButton
