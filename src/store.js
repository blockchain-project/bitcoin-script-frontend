import { createGlobalState } from "react-hooks-global-state"

const initialState = {
  net: "main",
}

const { useGlobalState } = createGlobalState(initialState)

export default useGlobalState
