import React from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import HomePage from "./pages/HomePage"
import SandboxPage from "./pages/SandboxPage"
import TransactionPage from "./pages/TransactionPage"

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route path="/transaction/:id">
          <TransactionPage />
        </Route>
        <Route path="/sandbox">
          <SandboxPage />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
