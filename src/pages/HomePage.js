import React from "react"
import TransactionInputCard from "../cards/TransactionInputCard"


function HomePage() {
  return (
    <div className={"container"}>
      <div className="row mt-3 mb-3">
        <div className="col">
          <TransactionInputCard />
        </div>
      </div>
    </div>
  )
}

export default HomePage
