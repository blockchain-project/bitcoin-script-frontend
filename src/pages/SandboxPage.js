import useAxios from "axios-hooks"
import PropTypes from "prop-types"
import queryString from "query-string"
import React, { useState } from "react"
import styled from "styled-components"
import SingleColumnPage from "../components/layout/CardPage"
import ErrorCard from "../components/layout/ErrorCard"
import LoadingCard from "../components/layout/LoadingCard"
import LightningErrorText from "../components/LightningErrorText"
import { ENDPOINTS } from "../const"
import SandboxState from "./SandboxState"

function SandboxPage() {
  // STATE
  const [step, setStep] = useState(0)
  // QUERY PARAM [required]
  const { opcode_str } = queryString.parse(window.location.search)

  // DATA: Fetch the StateMachine for the given opcode_str
  const [
    { data: states, loading: loadingStates, error: errorOnStates },
  ] = useAxios({
    url: ENDPOINTS.VISUALIZE_SCRIPT,
    method: "post",
    data: { script: opcode_str },
  })
  // DATA: Fetch the Script ([Opcode]) for the given opcode_str
  const [
    { data: script, loading: loadingScript, error: errorOnScript },
  ] = useAxios({
    url: ENDPOINTS.PARSE_OPCODE_STR,
    method: "post",
    data: { script: opcode_str },
  })

  // Loading & Error
  let error = errorOnScript || errorOnStates
  let loading = loadingScript || loadingStates

  if (loading) {
    return (
      <SingleColumnPage>
        <LoadingCard />
      </SingleColumnPage>
    )
  } else if (error || !states) {
    return (
      <SingleColumnPage>
        <ErrorCard text="Something went wrong" />
      </SingleColumnPage>
    )
  }

  // LOGIC: State Machine
  const hasNext = () => (states ? step < states.length - 1 : false)
  const hasPrevious = () => step > 0
  const isErrorState = () => (states ? states[step].error : false)
  const currentState = states ? states[step] : null
  console.log(currentState)

  return (
    <SingleColumnPage>
      <StyledCard className="card w-100">
        <div className="card-body d-flex flex-column">
          {/* Title */}
          <p className="card-title h4 ">Sandbox</p>

          {/* Machine State */}
          {currentState ? (
            <SandboxState
              className="flex-grow-1"
              script={script}
              state={currentState}
            />
          ) : null}

          {/* Error during execution */}
          {isErrorState() ? (
            <div className="flex-column mt-3">
              <LightningErrorText text={<i>Could not fully evaluate OP_CODE</i>} />
              <p>{currentState.error}</p>
            </div>
          ) : null}
        </div>

        {/* Machine Controls */}
        <div className="card-footer justify-content-between d-flex align-items-center">
          <button
            className="btn btn-secondary"
            disabled={!hasPrevious()}
            onClick={() => setStep(step - 1)}>
            Step Backwards
          </button>


          <button
            className="btn btn-primary"
            disabled={!hasNext()}
            onClick={() => setStep(step + 1)}>
            Step Forward
          </button>
        </div>
      </StyledCard>
    </SingleColumnPage>
  )
}

SandboxPage.propTypes = {
  opcode_str: PropTypes.string.isRequired,
}

const StyledCard = styled.div`
  height: calc(100vh - 2rem);
  max-height: 600px;
`

export default SandboxPage
