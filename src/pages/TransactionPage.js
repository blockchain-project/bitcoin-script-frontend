import React, { useEffect, useState } from "react"
import { useParams } from "react-router"
import TransactionHeaderCard from "../cards/TransactionHeaderCard"
import ScriptCard from "../cards/TransactionScriptsCard"
import { ENDPOINTS } from "../const"
import useGlobalState from "../store"

function TransactionPage(props) {
  let { id } = useParams()
  const [net] = useGlobalState("net")

  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(true)
  const [transaction, setTransaction] = useState(null)

  useEffect(() => {
    const getTransaction = async () => {
      setLoading(true)

      const response = await fetch(ENDPOINTS.TRANSACTION(id, net), {
        method: "GET",
      })

      if (response.status !== 200) {
        setError(true)
        setLoading(false)
      } else {
        const transaction = await response.json()
        setTransaction(transaction)
        setLoading(false)
        setError(false)
      }
    }
    getTransaction()
  }, [id, net])

  return (
    <div className={"container"}>
      <div className="row mt-3 mb-3">
        <div className="col">
          <TransactionHeaderCard {...{ transaction, loading, error }} />

          {!loading && !error ? <ScriptCard {...{ transaction, net }} /> : null}
        </div>
      </div>
    </div>
  )
}

TransactionPage.propTypes = {}

export default TransactionPage
