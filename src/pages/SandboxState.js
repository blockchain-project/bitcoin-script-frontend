import React from "react"
import PropTypes from "prop-types"

import styles from "./SandboxState.module.scss"

function SandboxState(props) {
  const { script, state } = props

  return (
    <div className={`container ${props.className}`}>
      <div className="row justify-content-between h-100">
        <Stack state={state} />
        <Script state={state} script={script} />
      </div>
    </div>
  )
}

const Script = (props) =>
  <div className="col-8 h-100 pr-0">
    <pre className={`${styles.script} h-100`}>
      {props.script.map((item, i) => (
        <Line
          key={i}
          lineNumber={i + 1}
          old={i < props.state.current_line}
          error={props.state.current_line === i && props.state.error}
          current={props.state.current_line === i && !props.state.error}
          code={item.code}
          data={item.data}
        />
      ))}
    </pre>
  </div>

const Line = (props) => {
  let type = `${props.old ? "old" : ""}${props.current ? "current" : ""}${props.error ? "error" : ""}`
  return (
    <span type={type}>
      {/* Line Number */}
      <span>{props.lineNumber}</span>
      {/* OP_CODE */}
      <span>{props.code}</span>
      {/* DATA */}
      {props.data.map((param, j) => (
        <span key={j}> {param}</span>
      ))}
    </span>
  )
}

const Stack = (props) =>
  <div className="col-4 h-100 pl-0">
    <pre className={styles.stack}>
      {props.state.stack.map((item, i) => {
        let reverseIndex = props.state.stack.length - (i + 1)
        let isPush = reverseIndex < props.state.push
        let isPop = reverseIndex + props.state.push < props.state.pop
        console.log(`reverse: ${reverseIndex} push: ${props.state.push} pop: ${props.state.pop}`)

        return <Item key={i} value={item} push={isPush} pop={isPop} />
      })}
    </pre>
  </div>

const Item = (props) => {
  let type = `${props.push ? "push":""}${props.pop?"pop":""}`
  return <span type={type.toString()}>{props.value}</span>
}


SandboxState.propTypes = {
  className: PropTypes.any,

  script: PropTypes.array.isRequired,
  state: PropTypes.object.isRequired,
}

export default SandboxState
