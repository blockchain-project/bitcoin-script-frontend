import PropTypes from "prop-types"
import React from "react"
import {
  Link
} from "react-router-dom"
import HoverableDescriptionItem from "../components/HoverableDescriptionItem"
import SingleColumnPage from "../components/layout/CardPage"
import ErrorCard from "../components/layout/ErrorCard"
import LoadingCard from "../components/layout/LoadingCard"
import NetBadge from "../components/NetBadge"
import useGlobalState from "../store"
import styles from "./TransactionHeaderCard.module.scss"
import {
  TOOLTIP_TRANSACTION_FEE,
  TOOLTIP_TRANSACTION_WEIGHT
} from "../const.js"


function TransactionHeaderCard(props) {
  const {
    transaction,
    loading,
    error
  } = props
  const [net] = useGlobalState("net")


  if (loading) {
    return (
      <SingleColumnPage>
        <LoadingCard />
      </SingleColumnPage>
    )
  }

  if (error && !loading) {
    return (
      <SingleColumnPage>
        <ErrorCard text="Transaction not found" />
      </SingleColumnPage>
    )
  }

  return (
    <div className={`w-100 card ${error ? "border-danger" : null}`}>
      <div className="card-body">
        {/* Card: Header */}
        <Link className={styles.title} to="/">
          {/* Card: Title */}
          <h4 className="card-title">
            {/* Button: Back Arrow */}
            <i className="bi bi-arrow-left" />
            {/* Title */}
            Transaction
            {/* Badge: MainNet | TestNet */}
            <NetBadge net={net} />
          </h4>
        </Link>
      </div>

      {/* Card: Content */}
      <ul className="list-group list-group-flush">
        <TransactionHash transaction={transaction} />
        <TransactionFee transaction={transaction} />
        <TransactionSize transaction={transaction} />
        <TransactionWeight transaction={transaction} />
        <TransactionStatus transaction={transaction} />
        <TransactionTimestamp transaction={transaction} />
        <TransactionBlock transaction={transaction} />
      </ul>

      <Footer />
    </div>
  )
}

const TransactionHash = (props) =>
  <HoverableDescriptionItem
    name="Hash"
    description={props.transaction.txid}
  />

const TransactionFee = (props) =>
  <HoverableDescriptionItem
    name="Fee"
    tooltip={TOOLTIP_TRANSACTION_FEE}
    description={`${props.transaction.fee * 0.00000001} BTC`} />

const TransactionSize = (props) =>
  <HoverableDescriptionItem
    name="Size"
    description={`${props.transaction.size} bytes`} />
const TransactionWeight = (props) =>
  <HoverableDescriptionItem
    name="Weight"
    tooltip={TOOLTIP_TRANSACTION_WEIGHT}
    description={props.transaction.weight} />
const TransactionStatus = (props) =>
  <HoverableDescriptionItem
    name="Status"
    description={
      <span className={`${props.transaction.status.confirmed
        && props.transaction.status.confirmed === true
        ? "text-success" : "text-warning"}`}>
        {props.transaction.status.confirmed ? "Confirmed" : "Not confirmed"}
      </span>
    } />
const TransactionTimestamp = (props) =>
  <HoverableDescriptionItem
    name="Timestamp"
    description={`${new Date(props.transaction.status.block_time)}`} />
const TransactionBlock = (props) =>
  <HoverableDescriptionItem
    name="Block"
    description={props.transaction.status.block_height ? props.transaction.status.block_height : "Mempool"} />

const Footer = () =>
  <div className="card-footer">
    <p className="card-text">
      <small className="text-muted">Fetched x minutes ago</small>
    </p>
  </div>

TransactionHeaderCard.propTypes = {
  transaction: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.bool,
}

export default TransactionHeaderCard
