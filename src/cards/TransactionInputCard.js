import React, {
  useState
} from "react"
import {
  Col,
  Form
} from "react-bootstrap"
import {
  useHistory
} from "react-router"
import LightningErrorText from "../components/LightningErrorText"
import SpinnerButton from "../components/SpinnerButton"
import {
  ENDPOINTS
} from "../const"
import useGlobalState from "../store"

function TransactionInputCard(props) {
  // Global state
  const [net, setNet] = useGlobalState("net")

  // State
  const [trxid, setTrxid] = useState("")
  const [validated, setValidated] = useState(false)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  const history = useHistory()

  // Functions
  const resetState = () => {
    setValidated(false)
    setLoading(true)
    setError(false)
  }

  const submitTransaction = () =>
    history.push(`/transaction/${trxid}`)

  const handleSetTrxId = (event) => {
    setTrxid(event.target.value)
    setValidated(false)

  }
  const handleSetNet = (event) => {
    setNet(event.target.value)
    setValidated(false)
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    event.stopPropagation()

    resetState()
    httpGetValidate()
  }

  const httpGetValidate = async () => {
    const response = await fetch(ENDPOINTS.VALIDATE(trxid, net), {
      method: "GET",
    })

    if (response.status === 200) {
      setError(false)
      submitTransaction()
    } else {
      setError(true)
    }

    setLoading(false)
    setValidated(true)
  }

  return (
    <div className="card w-100">
      <div className="card-body">
        {/* Title */}
        <p className="card-title h4">Bitcoin Script Decoder</p>

        {/* Text: Explanation of Website */}
        <div className="card-text">
          This site provides you with an easy overview of the Bitcoin scripts that build up your transaction. Using the Sandbox, you can run scripts and evaluate step-by-step how the script manipulates the stack.
          <br />
          This website was built as an educational tool. Currently, the support for Bitcoin OP_CODES is limited. Instructions that validate signatures do not actually validate signatures, and instructions related to control flow will not execute.
        </div>

        <Form noValidate onSubmit={handleSubmit}>
          <Form.Row>
            {/* Input: Transaction ID */}
            <Form.Group as={Col} sm={8}>
              <Form.Label><b>Transaction Id</b></Form.Label>

              <Form.Control
                id="form-trxid"
                isInvalid={error}
                onChange={handleSetTrxId}
                value={trxid}
                placeholder="f4184fc596403b9d638783cf57adfe4c75c605f6356fbc91338530e9831e9e16"
                disabled={loading}
              />

              <Form.Text className="text-muted">
                Please provide a valid Bitcoin transaction hash
              </Form.Text>
            </Form.Group>

            {/* Input: Net */}
            <Form.Group as={Col} sm={4}>
              <Form.Label><b>Net</b></Form.Label>

              <Form.Control
                id="form-net"
                as="select"
                value={net}
                onChange={handleSetNet}
                disabled={loading}
                custom>
                <option value="main">Main Net</option>
                <option value="test">Test Net</option>
              </Form.Control>
            </Form.Group>
          </Form.Row>

          {/* Input: Submit */}
          <div className="d-flex align-items-center">
            <TransactionNotFound show={!loading && error} />
            <LoadingButton loading={loading} />
          </div>
        </Form>
      </div>
    </div>
  )
}

const TransactionNotFound = (props) =>
  props.show && <LightningErrorText
    class="m-auto"
    text="Transaction not found"
  />

const LoadingButton = (props) =>
  <SpinnerButton
    variant="primary"
    type="submit"
    loading={props.loading}>
    Go!
  </SpinnerButton>


TransactionInputCard.propTypes = {}

export default TransactionInputCard
