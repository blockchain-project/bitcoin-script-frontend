import useAxios from "axios-hooks"
import PropTypes from "prop-types"
import React from "react"
import LightningErrorText from "../components/LightningErrorText"
import Script from "../components/script/Script"
import {
  ENDPOINTS
} from "../const"


function TransactionScriptsCard(props) {
  const {
    transaction,
    net
  } = props
  const [{
    data,
    loading,
    error
  }] = useAxios(ENDPOINTS.SCRIPTS(transaction.txid, net))

  return (
    <>
      <ScriptCard
        name="Input Scripts"
        loading={loading}
        error={error}
        runnable
        data={data && data["input_scripts"]}
      />

      <ScriptCard
        name="Output Scripts"
        loading={loading}
        error={error}
        data={data && data["output_scripts"]}
      />
    </>
  )
}

const Card = (props) =>
  <div className={`w-100 mt-3 card ${props.error ? "border-danger" : null}`}>
    {props.children}
  </div>

const ScriptCard = (props) =>
  <Card error={props.error}>
    <div className="card-body">
      <p className="h4 card-title m-0">{props.name}</p>

      <Error show={!props.loading && props.error} />
      <Spinner loading={props.loading} />
    </div>

    {!props.loading && !props.error && props.data ? (
      <ul className="list-group list-group-flush">
        {props.data.map((script, i) => (
          <li className="list-group-item" key={i}>
            <Script
              key={i}
              runnable={props.runnable}
              index={i}
              title={`Script ${i}`}
              header={`Script ${i} - ${script.classifier.classification}`}
              {...script}
            />
          </li>
        ))}
      </ul>
    ) : null}
  </Card>

const Error = (props) =>
  props.show &&
  <LightningErrorText
    class="mt-3"
    text="Failed to load scripts"
  />

const Spinner = (props) =>
  props.loading && <div className="d-flex justify-content-center mt-3">
    <span className="spinner-border" />
  </div>

TransactionScriptsCard.propTypes = {
  transaction: PropTypes.object,
}

export default TransactionScriptsCard
