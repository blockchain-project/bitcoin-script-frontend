# Bitcoin Classifier Frontend

This project provides the frontend for the bitcoin script classifier. This project was built using React [Create React App](https://github.com/facebook/create-react-app).

## Prequisites

To run this project, you need to have Node.js installed (https://nodejs.org/en/download/).

## Run the project

To setup this project:
1. Clone the repository
2. Run `npm install`
3. Start the server with `npm run start`

If `npm install` gives you errors, try removing both the `node_modules` directory, and the `package-lock.json` file.


By default this project runs on `localhost:3000`. You should also run the backend. By default the backend runs on port `8000`. If your backend is running on a different port, you can specify this in the `package.json` file.

## Screenshot

![Screenshot](.github/screenshot.png)
